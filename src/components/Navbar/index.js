import React, { Component } from 'react';
import './style.css';

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchKeyword: '',
      searchResults: [],
    }
  }

  handleSearch = (e) => {
    let value = e.target.value;
    this.setState({ searchKeyword: value });
    if (value.length === 0) this.setState({ searchKeyword: '', searchResults: [] });
    if (value.length > 2) {
      let searchResults = Products.filter( product => product.name.toLowerCase().indexOf(value.toLowerCase()) !== -1);
      console.log('searchResults : ', searchResults);
      this.setState({ searchResults });
    }
  }
  

  render() { 
    let {searchResults, searchKeyword} = this.state;
    return ( 
      <div className='navbar-wrapper'>
        <div className='logo-wrapper'>
          LOGO
        </div>
        <div className='search-wrapper'>
          <input 
            placeholder='Search Products...' 
            onChange={this.handleSearch}
            value={searchKeyword}
          />
          {/* CONDITIONAL RENDERING */}
          {
            searchResults.length ? 
            <div className='search-results-wrapper'>
              {
                searchResults.map( (product, i) => <div className='search-item' key={i}>{product.name}</div> )
              }
            </div> 
            : null
          }
        </div>
        <div className='user-details-wrapper'>
          Vaibhav
        </div>
        <div className='cart-wrapper'>
          <div className='cart'>
            <img src={require('../../assets/cart.svg')} alt='cart'/>
          </div>
        </div>
      </div>
    );
  }
}
 
export default Navbar;


const Products = [
  {id: 1, name: 'Black Shirt', price: '$20'},
  {id: 2, name: 'Blue Shirt', price: '$30'},
  {id: 3, name: 'Yellow Shirt', price: '$10'},
  {id: 4, name: 'Red Shirt', price: '$25'},
  {id: 5, name: 'Black Trousers', price: '$55'},
  {id: 6, name: 'Light Blue Chinos', price: '$75'},
  {id: 7, name: 'White T-Shirt', price: '$25'},
]