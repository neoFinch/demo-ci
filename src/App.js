import React, {useState, useEffect} from 'react';
import logo from './logo.svg';
import './App.css';
import Navbar from './components/Navbar';

const Products = [
  {id: 1, name: 'Black Shirt', price: '$20'},
  {id: 2, name: 'Blue Shirt', price: '$30'},
  {id: 3, name: 'Yellow Shirt', price: '$10'},
  {id: 4, name: 'Red Shirt', price: '$25'},
  {id: 5, name: 'Black Trousers', price: '$55'},
  {id: 6, name: 'Light Blue Chinos', price: '$75'},
  {id: 7, name: 'White T-Shirt', price: '$25'},
]

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      register: {
        name: '',
        email: '',
        pwd: '',
        status: false
      },
      questions: [
        { id: 1, 
          ques: 'What is ur name', 
          options: [
            {id: 'a', value: 'Vaibhav'}, 
            {id: 'b', value: 'Naresh'}, 
            {id: 'c', value: 'Hemant'}, 
          ],
          correctAns: 'c'
        },
        { id: 2, 
          ques: 'What is ur name', 
          options: [
            {id: 'a', value: 'Vaibhav'}, 
            {id: 'b', value: 'Naresh'}, 
            {id: 'c', value: 'Hemant'}, 
          ],
          correctAns: 'c'
        }
      ],
      userAns: []
    }
  }


  handleInput = (e, type) => {
    let { value } = e.target;
    let { register } = this.state;

    if (type === 'name') {
      register.name = value;
    } 
    if (type === 'email') {
      register.email = value;
    } 
    if (type === 'pwd') {
      register.pwd = value;
    } 
    this.setState({ register });

  } 


  handleForm = () => {
    // validations
    // API
    // response 
    // redirect
    let { register } = this.state;
    register.status = true;
    this.setState({ register });
  }

  handleOption = (e, ques_id) => {
    console.log(ques_id);
    console.log(e.target.value);
    let { userAns } = this.state;
    
  }
  
  render() {
    return(
      <div className="App" style={{padding: '20px'}}>
        <Navbar />
        {
          this.state.register.status 
          ?
          <div>
            Questions
            {
              this.state.questions.map(ques => 
              <div>
                <label>{ques.ques}</label>
                <ul>
                  {
                    ques.options.map((option, index) => 
                    <div key={option.id}>
                      <input 
                        id={`${ques.id}-${option.id}`}
                        type='radio' 
                        name={`ques-${ques.id}`}
                        onChange={(e) => this.handleOption(e, option.id)}
                        value={option.value} 
                      />
                      <label htmlFor={`${ques.id}-${option.id}`}>{option.value}</label>
                    </div>)
                  }
                </ul>
              </div>
            )
            }
          </div>
          :
          <div>
            <h3>Register</h3>
            <div><input onChange={(event) => this.handleInput(event, 'name')} name='fname' type='text' placeholder='name' /></div>
            <div><input onChange={(event) => this.handleInput(event, 'email')} name='email' type='text' placeholder='email' /></div>
            <div><input onChange={(event) => this.handleInput(event, 'pwd')} name='pwd' type='password' placeholder='pwd' /></div>
            <button onClick={this.handleForm}> Submit </button>
          </div>
        }
        

      </div>
    )
  }
}
 
export default App;
